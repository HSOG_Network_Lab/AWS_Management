/**
 * list all running EC2 instances in all regions
 * same for loadbalancers, ebsVolumes and snapshots
 * stop them if wanted by user
 * @author Hendrik Schutter
 * @version 07.05.2021
 */

package com.amazonaws.cloud.lab.manage;

import java.util.ArrayList;
import java.util.Scanner;

import com.amazonaws.cloud.lab.manage.awsresources.ActiveEC2Instance;
import com.amazonaws.cloud.lab.manage.awsresources.KeyPair;
import com.amazonaws.cloud.lab.manage.awsresources.LoadBalancers;
import com.amazonaws.cloud.lab.manage.awsresources.Volumes;
import com.amazonaws.cloud.lab.manage.worker.InstancesWorkerThread;
import com.amazonaws.cloud.lab.manage.worker.KeysWorkerThread;
import com.amazonaws.cloud.lab.manage.worker.LoadBalancersWorkerThread;
import com.amazonaws.cloud.lab.manage.worker.VolumesWorkerThread;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;

import com.amazonaws.services.ec2.model.DescribeRegionsResult;
import com.amazonaws.services.ec2.model.Region;

public class CleanResources
{
	public static ArrayList<Region> ec2Regions = null;

	public static AmazonEC2 ec2Global = null;

	public static Scanner scanner = new Scanner(System.in);

	// all ec2 instances stopped or running
	public static ArrayList<ActiveEC2Instance> ec2ActivInstancesList = null;

	// all key pairs
	public static ArrayList<KeyPair> keypairList = null;

	// all volumes
	public static ArrayList<Volumes> volumesList = null;

	// all load balancers
	public static ArrayList<LoadBalancers> loadBalancersList = null;

	public static void main(String[] args)
	{
		ec2Regions = new ArrayList<Region>();

		ec2Global = AmazonEC2ClientBuilder.standard().build();

		ec2ActivInstancesList = new ArrayList<ActiveEC2Instance>();
		keypairList = new ArrayList<KeyPair>();
		volumesList = new ArrayList<Volumes>();
		loadBalancersList = new ArrayList<LoadBalancers>();

		findRegions(ec2Global);

		boolean instanceThreadIsFinished = false;
		InstancesWorkerThread threadInstances = new InstancesWorkerThread();
		threadInstances.start();

		boolean volumeThreadIsFinished = false;
		VolumesWorkerThread threadVolumes = new VolumesWorkerThread();
		threadVolumes.start();

		boolean keysThreadIsFinished = false;
		KeysWorkerThread threadKeys = new KeysWorkerThread();
		threadKeys.start();

		boolean loadBalancersThreadIsFinished = false;
		LoadBalancersWorkerThread threadLoadBalancers = new LoadBalancersWorkerThread();
		threadLoadBalancers.start();

		while (true) {

			try {
				threadInstances.join(300, 0);
				if ((threadInstances.isAlive() == false)
						&& !instanceThreadIsFinished) {
					instanceThreadIsFinished = true;
					if (ec2ActivInstancesList.size() > 0) {
						ActiveEC2Instance.handleEC2Instances(ec2ActivInstancesList);
					} else {
						System.out.println("No EC2 Instances found!");
						System.out.flush();
					}
				}

			} catch (InterruptedException e) {
				System.err.println("Thread for listing Instances is chrashed!");
			}

			try {
				threadVolumes.join(100, 0);
				if ((threadVolumes.isAlive() == false) && !volumeThreadIsFinished) {
					volumeThreadIsFinished = true;
					if (volumesList.size() > 0) {
						Volumes.handleVolumes(volumesList);
					} else {
						System.out.println("No volumes found!");
						System.out.flush();
					}
				}

			} catch (InterruptedException e) {
				System.err.println("Thread for listing Volumes is chrashed!");
			}

			try {
				threadKeys.join(100, 0);
				if ((threadKeys.isAlive() == false) && !keysThreadIsFinished) {
					keysThreadIsFinished = true;
					if (keypairList.size() > 0) {
						KeyPair.handleKeyPairs(keypairList);
					} else {
						System.out.println("No key pairs found!");
						System.out.flush();
					}
				}

			} catch (InterruptedException e) {
				System.err.println("Thread for listing Keys is chrashed!");
			}

			try {
				threadLoadBalancers.join(100, 0);
				if ((threadLoadBalancers.isAlive() == false)
						&& !loadBalancersThreadIsFinished) {
					loadBalancersThreadIsFinished = true;
					if (loadBalancersList.size() > 0) {
						LoadBalancers.handleLoadBalancers(loadBalancersList);
					} else {
						System.out.println("No load balancers found!");
						System.out.flush();
					}
				}

			} catch (InterruptedException e) {
				System.err
						.println("Thread for listing load balancers is chrashed!");
			}

			if (instanceThreadIsFinished && keysThreadIsFinished
					&& volumeThreadIsFinished && loadBalancersThreadIsFinished) {
				// all worker are finished
				break;
			}

			if (!instanceThreadIsFinished && !volumeThreadIsFinished
					&& !keysThreadIsFinished && !loadBalancersThreadIsFinished) {
				// no worker is finished yet
				System.out.print(".");
				System.out.flush();
			}
		}

		System.out.flush();
		System.out.println("exit");
	}

	private static void findRegions(AmazonEC2 ec2)
	{
		DescribeRegionsResult regions_response = ec2.describeRegions();
		for (Region region : regions_response.getRegions()) {
			System.out.println("Found region " + region.getRegionName());

			/*
			 * if(region.getRegionName().equals("us-west-2")) {
			 * ec2Regions.add(region); }
			 */
			ec2Regions.add(region);
		}
	}

}
