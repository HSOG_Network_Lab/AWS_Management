package com.amazonaws.cloud.lab.manage.awsresources;

import java.util.ArrayList;

import com.amazonaws.cloud.lab.manage.CleanResources;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Region;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;

public class ActiveEC2Instance
{
	enum UserAction {
		STOP, TERMINATE, SKIP
	}

	enum InstanceState {
		RUNNING, STOPPED, TERMINATED
	}

	private InstanceState state;

	private Instance instance;

	private Region region;

	public ActiveEC2Instance(Instance instance, Region region, boolean isRunning)
	{
		this.instance = instance;
		this.region = region;
		this.state = (isRunning) ? InstanceState.RUNNING : InstanceState.STOPPED;
	}

	public Instance getInstance()
	{
		return instance;
	}

	public Region getRegion()
	{
		return region;
	}

	public InstanceState getState()
	{
		return this.state;
	}

	/**
	 * stop ec2 instance
	 */
	private void stop()
	{
		if (this.getState() == InstanceState.RUNNING) {
			AmazonEC2 ec2Region = AmazonEC2ClientBuilder.standard()
					.withRegion(this.getRegion().getRegionName()).build();
			StopInstancesRequest request = new StopInstancesRequest()
					.withInstanceIds(this.getInstance().getInstanceId());
			ec2Region.stopInstances(request);
			System.out.println("Stopped: " + this.instance.getInstanceId());
			this.state = InstanceState.STOPPED;
		}
	}

	/**
	 * terminate ec2 instance
	 */
	private void terminate()
	{
		AmazonEC2 ec2Region = AmazonEC2ClientBuilder.standard()
				.withRegion(this.getRegion().getRegionName()).build();
		TerminateInstancesRequest request = new TerminateInstancesRequest()
				.withInstanceIds(this.getInstance().getInstanceId());
		ec2Region.terminateInstances(request);
		System.out.println("Terminated: " + this.instance.getInstanceId());
		this.state = InstanceState.TERMINATED;
	}

	/**
	 * ask user to terminate or stop a instance
	 */
	public static void handleEC2Instances(
			ArrayList<ActiveEC2Instance> ec2ActivInstancesList)
	{
		boolean next = true;

		while (next) {
			System.out.println("\nFound following EC2 Instances: ");

			// loop through instances
			for (ActiveEC2Instance activeEC2Instance : ec2ActivInstancesList) {
				if (activeEC2Instance.getState() != InstanceState.TERMINATED) {
					// print only running and stopped instances
					System.out.println(activeEC2Instance.getRegion().getRegionName()
							+ ": " + activeEC2Instance.getInstance().getInstanceId()
							+ " "
							+ ((activeEC2Instance.getState() == InstanceState.RUNNING)
									? "Running"
									: "Stopped"));
				}
			}
			// ask user: stop or terminate
			switch (onlyStopInstances()) {
			case STOP:
				stopInstances(ec2ActivInstancesList);
				break;
			case TERMINATE:
				terminateInstances(ec2ActivInstancesList);
				break;
			case SKIP:
				next = false;
				break;
			default:
				break;
			}

			boolean atLeastOneNonTerminatedInstrance = false;
			// check if a runnning or stopped instance is still left in list
			for (ActiveEC2Instance activeEC2Instance : ec2ActivInstancesList) {
				if (activeEC2Instance.getState() != InstanceState.TERMINATED) {
					atLeastOneNonTerminatedInstrance = true;
				}
			}

			if (atLeastOneNonTerminatedInstrance == false) {
				next = false;
				// no instances to stop or terminate left --> return
			}
		}
	}

	/**
	 * get user decision (stop or terminate)
	 */
	public static UserAction onlyStopInstances()
	{
		System.out.println(
				"\nDo you want to stop (s) or terminate (t) one or more instances? Type (skip) for skipping EC2 instances.");

		while (true) {

			String userinput = CleanResources.scanner.next();
			if (userinput.equals("s")) {
				return UserAction.STOP;
			}

			if (userinput.equals("t")) {
				return UserAction.TERMINATE;
			}

			if (userinput.equals("skip")) {
				return UserAction.SKIP;
			}

			System.out
					.println("Enter s for stopping or t for termination or skip");
		}
	}

	/**
	 * ask user witch instance will be stopped
	 */
	public static void stopInstances(
			ArrayList<ActiveEC2Instance> ec2ActivInstancesList)
	{
		boolean stoppedAtLeastOneInstance = false;
		System.out
				.println("Enter instances ID or Region to stop these instances");

		String userinput = CleanResources.scanner.next();

		for (ActiveEC2Instance activeEC2Instance : ec2ActivInstancesList) {
			if (((userinput.equals(activeEC2Instance.getInstance().getInstanceId())
					|| (userinput
							.equals(activeEC2Instance.getRegion().getRegionName())))
					&& (activeEC2Instance.getState() == InstanceState.RUNNING))) {
				// found running instance that is selected based on ID or Region
				activeEC2Instance.stop();
				stoppedAtLeastOneInstance = true;
				System.out.println("stopping: "
						+ activeEC2Instance.getInstance().getInstanceId());
			}
		}
		if (stoppedAtLeastOneInstance == false) {
			System.out
					.println("Unable to find running instance(s) with this input!");
		}
	}

	/**
	 * ask user which instance will be stopped
	 */
	public static void terminateInstances(
			ArrayList<ActiveEC2Instance> ec2ActivInstancesList)
	{
		boolean terminatedAtLeastOneInstance = false;
		System.out
				.println("Enter instances ID or Region to stop these instances");

		String userinput = CleanResources.scanner.next();

		for (ActiveEC2Instance activeEC2Instance : ec2ActivInstancesList) {
			if ((userinput.equals(activeEC2Instance.getInstance().getInstanceId())
					|| (userinput
							.equals(activeEC2Instance.getRegion().getRegionName())))) {
				// found instance that is selected based on ID or Region
				activeEC2Instance.terminate();
				terminatedAtLeastOneInstance = true;
				System.out.println("terminating: "
						+ activeEC2Instance.getInstance().getInstanceId());
			}
		}
		if (terminatedAtLeastOneInstance == false) {
			System.out.println("Unable to find instance(s) with this input!");
		}
	}

}
