package com.amazonaws.cloud.lab.manage.awsresources;

import java.util.ArrayList;

import com.amazonaws.cloud.lab.manage.CleanResources;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DeleteKeyPairRequest;
import com.amazonaws.services.ec2.model.DeleteKeyPairResult;
import com.amazonaws.services.ec2.model.KeyPairInfo;
import com.amazonaws.services.ec2.model.Region;


public class KeyPair
{

	enum UserAction {
		DELETE, SKIP
	}

	private KeyPairInfo keypair;
	private Region region;
	private boolean isDeleted = false;

	public KeyPair(KeyPairInfo keypair, Region region)
	{
		this.keypair = keypair;
		this.region = region;
		this.isDeleted = false;
	}

	public KeyPairInfo getKeyPair()
	{
		return keypair;
	}

	public Region getRegion()
	{
		return region;
	}

	public boolean isDeletet()
	{
		return this.isDeleted;
	}

	/**
	 * delete key pair
	 */
	private void delete()
	{
			AmazonEC2 ec2Region = AmazonEC2ClientBuilder.standard().withRegion(this.getRegion().getRegionName()).build();
			DeleteKeyPairRequest request = new DeleteKeyPairRequest().withKeyName(keypair.getKeyName());
			@SuppressWarnings("unused")
			DeleteKeyPairResult response = ec2Region.deleteKeyPair(request);
			System.out.println("Deleted: " + keypair.getKeyName());
			isDeleted = true;
	}

	/**
	 * ask user to delete or skip 
	 */
	public static void handleKeyPairs(ArrayList<KeyPair> keyPairsList)
	{
		boolean next = true;

		while (next) {
			System.out.println("\nFound following KeyPairs: ");

			// loop through key pairs
			for (KeyPair keypair : keyPairsList) {
				
				
				if (keypair.isDeleted == false) {
					// print only on deleted key pairs
					System.out.println(keypair.getRegion().getRegionName()
							+ ": " + keypair.getKeyPair().getKeyName());
				}
			}
			// ask user: delete or skip
			switch (askUserForDelete()) {
			case DELETE:
				deleteKeyPairs(keyPairsList);
				break;
			case SKIP:
				next = false;
				break;
			default:
				break;
			}

			boolean atLeastOneNonDeletedKeyPair = false;
			// check if a key pair is still left in list
			for (KeyPair tmpkeypair : keyPairsList) {
				if (tmpkeypair.isDeleted == false) {
					atLeastOneNonDeletedKeyPair = true;
				}
			}

			if (atLeastOneNonDeletedKeyPair == false) {
				next = false;
				// no key pairs to delete left left --> return
			}
		}
	}

	/**
	 * get user decision (delete or skip)
	 */
	public static UserAction askUserForDelete()
	{
		System.out.println(
				"\nDo you want to delete (d) one or more key pairs? Type (skip) for skipping key pairs.");

		while (true) {

			String userinput = CleanResources.scanner.next();
			if (userinput.equals("d")) {
				return UserAction.DELETE;
			}

			if (userinput.equals("skip")) {
				return UserAction.SKIP;
			}

			System.out
					.println("Enter d for delete or skip");
		}
	}

	/**
	 * ask user which key pairs  will be deleted
	 */
	public static void deleteKeyPairs(
			ArrayList<KeyPair> keyPairsList)
	{
		boolean deletedAtLeastOneKeyPair = false;
		System.out
				.println("Enter key pair name or Region to delete these key pairs");

		String userinput = CleanResources.scanner.next();

		for (KeyPair tmpkeypair : keyPairsList) {
			if (((userinput.equals(tmpkeypair.getKeyPair().getKeyName())
					|| (userinput
							.equals(tmpkeypair.getRegion().getRegionName())))
					&& (tmpkeypair.isDeleted == false))) {
				// found key pair that is selected based on ID or Region
				tmpkeypair.delete();
				deletedAtLeastOneKeyPair = true;
				System.out.println("deleting: "+ tmpkeypair.getKeyPair().getKeyName());
			}
		}
		if (deletedAtLeastOneKeyPair == false) {
			System.out
					.println("Unable to find key pair(s) with this input!");
		}
	}

	
}
