package com.amazonaws.cloud.lab.manage.awsresources;

import java.util.ArrayList;
import com.amazonaws.cloud.lab.manage.CleanResources;
import com.amazonaws.services.ec2.model.Region;
import com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancing;
import com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancingClientBuilder;
import com.amazonaws.services.elasticloadbalancing.model.DeleteLoadBalancerRequest;
import com.amazonaws.services.elasticloadbalancing.model.DeleteLoadBalancerResult;
import com.amazonaws.services.elasticloadbalancing.model.LoadBalancerDescription;

public class LoadBalancers
{

	enum UserAction {
		DELETE, SKIP
	}

	private LoadBalancerDescription loadbalancer;

	private Region region;

	private boolean isDeleted = false;

	public LoadBalancers(LoadBalancerDescription loadbalancer, Region region)
	{
		this.loadbalancer = loadbalancer;
		this.region = region;
		this.isDeleted = false;
	}

	public LoadBalancerDescription getLoadBalancer()
	{
		return loadbalancer;
	}

	public Region getRegion()
	{
		return region;
	}

	public boolean isDeletet()
	{
		return this.isDeleted;
	}

	/**
	 * delete load balancer
	 */
	private void delete()
	{
		AmazonElasticLoadBalancing elasticRegion = AmazonElasticLoadBalancingClientBuilder.standard().withRegion(this.getRegion().getRegionName()).build();
		DeleteLoadBalancerRequest request = new DeleteLoadBalancerRequest().withLoadBalancerName(loadbalancer.getLoadBalancerName());
		@SuppressWarnings("unused")
		DeleteLoadBalancerResult responde = elasticRegion.deleteLoadBalancer(request);
		System.out.println("Deleted: " + loadbalancer.getDNSName());
		isDeleted = true;
	}

	/**
	 * ask user to delete or skip
	 */
	public static void handleLoadBalancers(ArrayList<LoadBalancers> loadBalancersList)
	{
		boolean next = true;

		while (next) {
			System.out.println("\nFound following load balancers: ");

			// loop through load balancers
			for (LoadBalancers lobal : loadBalancersList) {

				if (lobal.isDeleted == false) {

					// print only non deleted load balancers
					System.out.println(lobal.getRegion().getRegionName() + ": "
							+ lobal.getLoadBalancer().getDNSName());
				}
			}
			// ask user: delete or skip
			switch (askUserForDelete()) {
			case DELETE:
				deleteLoadBalancers(loadBalancersList);
				break;
			case SKIP:
				next = false;
				break;
			default:
				break;
			}

			boolean atLeastOneNonDeletedLoadBalancer = false;
			// check if a load balancer is still left in list
			for (LoadBalancers tmplobal : loadBalancersList) {
				if (tmplobal.isDeleted == false) {
					atLeastOneNonDeletedLoadBalancer = true;
				}
			}

			if (atLeastOneNonDeletedLoadBalancer == false) {
				next = false;
				// no load balancers to delete left --> return
			}
		}
	}

	/**
	 * get user decision (delete or skip)
	 */
	public static UserAction askUserForDelete()
	{
		System.out.println(
				"\nDo you want to delete (d) one or more load balancers? Type (skip) for skipping load balancers.");

		while (true) {

			String userinput = CleanResources.scanner.next();
			if (userinput.equals("d")) {
				return UserAction.DELETE;
			}

			if (userinput.equals("skip")) {
				return UserAction.SKIP;
			}

			System.out.println("Enter d for delete or skip");
		}
	}

	/**
	 * ask user which load balancers will be deleted
	 */
	public static void deleteLoadBalancers(ArrayList<LoadBalancers> loadbalancerList)
	{
		boolean deletedAtLeastOneLoadBalancer = false;
		System.out
				.println("Enter load balancer name or Region to delete these load balancer");

		String userinput = CleanResources.scanner.next();

		for (LoadBalancers tmplobal : loadbalancerList) {
			if (((userinput.equals(tmplobal.getLoadBalancer().getDNSName())
					|| (userinput.equals(tmplobal.getRegion().getRegionName())))
					&& (tmplobal.isDeleted == false))) {
				// found volume that is selected based on ID or Region
				tmplobal.delete();
				deletedAtLeastOneLoadBalancer = true;
				System.out
						.println("deleting: " + tmplobal.getLoadBalancer().getDNSName());
			}
		}
		if (deletedAtLeastOneLoadBalancer == false) {
			System.out.println("Unable to find load balancers(s) with this input!");
		}
	}

}
