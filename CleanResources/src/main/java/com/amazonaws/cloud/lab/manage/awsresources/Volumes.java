package com.amazonaws.cloud.lab.manage.awsresources;

import java.util.ArrayList;
import com.amazonaws.cloud.lab.manage.CleanResources;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DeleteVolumeRequest;
import com.amazonaws.services.ec2.model.DeleteVolumeResult;
import com.amazonaws.services.ec2.model.Region;
import com.amazonaws.services.ec2.model.Volume;

public class Volumes
{

	enum UserAction {
		DELETE, SKIP
	}

	private Volume volume;

	private Region region;

	private boolean isDeleted = false;

	public Volumes(Volume volume, Region region)
	{
		this.volume = volume;
		this.region = region;
		this.isDeleted = false;
	}

	public Volume getVolume()
	{
		return volume;
	}

	public Region getRegion()
	{
		return region;
	}

	public boolean isDeletet()
	{
		return this.isDeleted;
	}

	/**
	 * delete volume
	 */
	private void delete()
	{
		AmazonEC2 ec2Region = AmazonEC2ClientBuilder.standard()
				.withRegion(this.getRegion().getRegionName()).build();
		DeleteVolumeRequest request = new DeleteVolumeRequest()
				.withVolumeId(this.volume.getVolumeId());
		@SuppressWarnings("unused")
		DeleteVolumeResult response = ec2Region.deleteVolume(request);
		System.out.println("Deleted: " + volume.getVolumeId());
		isDeleted = true;
	}

	/**
	 * ask user to delete or skip
	 */
	public static void handleVolumes(ArrayList<Volumes> volumesList)
	{
		boolean next = true;

		while (next) {
			System.out.println("\nFound following volumes: ");

			// loop through volumes
			for (Volumes vol : volumesList) {

				if (vol.isDeleted == false) {

					// print only non deleted volumes
					System.out.println(vol.getRegion().getRegionName() + ": "
							+ vol.getVolume().getVolumeId());
				}
			}
			// ask user: delete or skip
			switch (askUserForDelete()) {
			case DELETE:
				deleteVolumes(volumesList);
				break;
			case SKIP:
				next = false;
				break;
			default:
				break;
			}

			boolean atLeastOneNonDeletedVolume = false;
			// check if a volume is still left in list
			for (Volumes tmpvolume : volumesList) {
				if (tmpvolume.isDeleted == false) {
					atLeastOneNonDeletedVolume = true;
				}
			}

			if (atLeastOneNonDeletedVolume == false) {
				next = false;
				// no volumes to delete left --> return
			}
		}
	}

	/**
	 * get user decision (delete or skip)
	 */
	public static UserAction askUserForDelete()
	{
		System.out.println(
				"\nDo you want to delete (d) one or more volumes? Type (skip) for skipping volumes.");

		while (true) {

			String userinput = CleanResources.scanner.next();
			if (userinput.equals("d")) {
				return UserAction.DELETE;
			}

			if (userinput.equals("skip")) {
				return UserAction.SKIP;
			}

			System.out.println("Enter d for delete or skip");
		}
	}

	/**
	 * ask user which key pairs will be deleted
	 */
	public static void deleteVolumes(ArrayList<Volumes> volumesList)
	{
		boolean deletedAtLeastOneKeyPair = false;
		System.out
				.println("Enter key pair name or Region to delete these volumes");

		String userinput = CleanResources.scanner.next();

		for (Volumes tmpvolumes : volumesList) {
			if (((userinput.equals(tmpvolumes.getVolume().getVolumeId())
					|| (userinput.equals(tmpvolumes.getRegion().getRegionName())))
					&& (tmpvolumes.isDeleted == false))) {
				// found volume that is selected based on ID or Region
				tmpvolumes.delete();
				deletedAtLeastOneKeyPair = true;
				System.out
						.println("deleting: " + tmpvolumes.getVolume().getVolumeId());
			}
		}
		if (deletedAtLeastOneKeyPair == false) {
			System.out.println("Unable to find volumes(s) with this input!");
		}
	}

}
