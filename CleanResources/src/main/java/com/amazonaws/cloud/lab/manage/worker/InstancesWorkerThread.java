package com.amazonaws.cloud.lab.manage.worker;

import com.amazonaws.cloud.lab.manage.CleanResources;
import com.amazonaws.cloud.lab.manage.awsresources.ActiveEC2Instance;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Region;
import com.amazonaws.services.ec2.model.Reservation;

public class InstancesWorkerThread extends Thread
{

	public void run()
	{
		for (Region region : CleanResources.ec2Regions) {
			boolean done = false;

			AmazonEC2 ec2Region = AmazonEC2ClientBuilder.standard()
					.withRegion(region.getRegionName()).build();

			DescribeInstancesRequest request = new DescribeInstancesRequest();
			while (!done) {
				DescribeInstancesResult response = ec2Region
						.describeInstances(request);
				for (Reservation reservation : response.getReservations()) {
					for (Instance instance : reservation.getInstances()) {

						if (instance.getState().getName().equals("running")) {
							CleanResources.ec2ActivInstancesList
									.add(new ActiveEC2Instance(instance, region, true));

						} else if (instance.getState().getName().equals("stopped")) {
							// not running
							CleanResources.ec2ActivInstancesList
									.add(new ActiveEC2Instance(instance, region, false));
						}
					}
				}
				request.setNextToken(response.getNextToken());

				if (response.getNextToken() == null) {
					done = true;
				}
			}
		}
	}
}
