package com.amazonaws.cloud.lab.manage.worker;

import com.amazonaws.cloud.lab.manage.CleanResources;
import com.amazonaws.cloud.lab.manage.awsresources.KeyPair;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeKeyPairsRequest;
import com.amazonaws.services.ec2.model.DescribeKeyPairsResult;
import com.amazonaws.services.ec2.model.KeyPairInfo;
import com.amazonaws.services.ec2.model.Region;

public class KeysWorkerThread extends Thread
{

	public void run()
	{
		for (Region region : CleanResources.ec2Regions) {
			AmazonEC2 ec2Region = AmazonEC2ClientBuilder.standard()
					.withRegion(region.getRegionName()).build();
			DescribeKeyPairsRequest request = new DescribeKeyPairsRequest();
			DescribeKeyPairsResult responese = ec2Region.describeKeyPairs(request);
			for (KeyPairInfo keyPair : responese.getKeyPairs()) {
				CleanResources.keypairList.add(new KeyPair(keyPair, region));
			}
		}
	}
}
