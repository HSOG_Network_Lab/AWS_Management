package com.amazonaws.cloud.lab.manage.worker;

import com.amazonaws.cloud.lab.manage.CleanResources;
import com.amazonaws.cloud.lab.manage.awsresources.LoadBalancers;
import com.amazonaws.services.ec2.model.Region;
import com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancing;
import com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancingClient;
import com.amazonaws.services.elasticloadbalancing.model.LoadBalancerDescription;

public class LoadBalancersWorkerThread extends Thread
{
	public void run()
	{
		for (Region region : CleanResources.ec2Regions) {
			AmazonElasticLoadBalancing elbv2 = AmazonElasticLoadBalancingClient
					.builder().withRegion(region.getRegionName()).build();
			com.amazonaws.services.elasticloadbalancing.model.DescribeLoadBalancersResult response = elbv2
					.describeLoadBalancers();
			for (LoadBalancerDescription lb : response
					.getLoadBalancerDescriptions()) {
				CleanResources.loadBalancersList.add(new LoadBalancers(lb, region));
			}
		}
	}
}
