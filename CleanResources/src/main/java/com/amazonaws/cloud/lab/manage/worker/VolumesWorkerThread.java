package com.amazonaws.cloud.lab.manage.worker;

import com.amazonaws.cloud.lab.manage.CleanResources;
import com.amazonaws.cloud.lab.manage.awsresources.Volumes;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeVolumesRequest;
import com.amazonaws.services.ec2.model.DescribeVolumesResult;
import com.amazonaws.services.ec2.model.Region;
import com.amazonaws.services.ec2.model.Volume;

public class VolumesWorkerThread extends Thread
{

	public void run()
	{
		for (Region region : CleanResources.ec2Regions) {
			AmazonEC2 ec2Region = AmazonEC2ClientBuilder.standard()
					.withRegion(region.getRegionName()).build();
			DescribeVolumesRequest request = new DescribeVolumesRequest();
			DescribeVolumesResult responese = ec2Region.describeVolumes(request);

			for (Volume vol : responese.getVolumes()) {
				//add only non attached volumes
				if (vol.getAttachments().size() == 0) {
					CleanResources.volumesList.add(new Volumes(vol, region));
				}
			}
		}
	}
}
