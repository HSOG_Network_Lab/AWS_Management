# AWS Management

This repository contains tools for monitoring and managing AWS resouces for the laboratory "Advanced Networking".

## ShowResources
Lists following AWS resouces in all regions:
- EC2 Instances (with state)
- Load Balancers
- Volumes
- Key-Pairs

Lists all resouces with their name/id in their region.


## CleanResources
Able to delete following AWS resouces:
- EC2 Instances
- Load Balancers
- Volumes
- Key-Pairs

User can delete resouces based on the name/id or the their region.

## Build
Maven is required. 
run `bash deplpy_all.sh` to build all tool as *.jar with dependencies
The generated *.jar is located in the target folder.

## Run
Make sure your AWS cli/sdk environment is setup correctly.
This is needed:

For GNU/Linux: `nano ~/.aws/credential`

For Windows: `C:\Users\USER\.aws\credential`

```
[default]
aws_access_key_id = XXXXXXXXXXXXXXXXXXX

aws_secret_access_key = XXXXXXXXXXXXXXX 
```

For GNU/Linux: `nano ~/.aws/config`

For Windows: `C:\Users\USER\.aws\config`

```
[default]
region = us-west-2
output = text 
```

