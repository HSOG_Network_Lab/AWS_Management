/**
 * list all running EC2 instances in all regions
 * same for loadbalancers, ebsVolumes and snapshots
 * stop them if wanted by user
 * @author Hendrik Schutter
 * @version 07.05.2021
 */

package com.amazonaws.cloud.lab.manage;

import java.util.ArrayList;

import com.amazonaws.cloud.lab.manage.worker.InstancesWorkerThread;
import com.amazonaws.cloud.lab.manage.worker.KeysWorkerThread;
import com.amazonaws.cloud.lab.manage.worker.LoadBalancersWorkerThread;
import com.amazonaws.cloud.lab.manage.worker.VolumesWorkerThread;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;

import com.amazonaws.services.ec2.model.DescribeRegionsResult;

import com.amazonaws.services.ec2.model.Region;

public class ShowResources
{
	public static ArrayList<Region> ec2Regions = null;

	// all instances stopped or running
	public static AmazonEC2 ec2Global = null;

	// results stored in list for each thread
	public static ArrayList<String> resultsInstancesList = new ArrayList<String>();
	public static ArrayList<String> resultsVolumesList = new ArrayList<String>();
	public static ArrayList<String> resultsKeysList = new ArrayList<String>();
	public static ArrayList<String> resultsLoadBalancersList = new ArrayList<String>();


	public static void main(String[] args)
	{
		ec2Regions = new ArrayList<Region>();

		ec2Global = AmazonEC2ClientBuilder.standard().build();

		findRegions(ec2Global);

		boolean instanceThreadIsFinished = false;
		InstancesWorkerThread threadInstances = new InstancesWorkerThread();
		threadInstances.start();

		boolean volumeThreadIsFinished = false;
		VolumesWorkerThread threadVolumes = new VolumesWorkerThread();
		threadVolumes.start();

		boolean keysThreadIsFinished = false;
		KeysWorkerThread threadKeys = new KeysWorkerThread();
		threadKeys.start();
		
		boolean loadBalancersThreadIsFinished = false;
		LoadBalancersWorkerThread threadLoadBalancers = new LoadBalancersWorkerThread();
		threadLoadBalancers.start();

		while (true) {

			try {
				threadInstances.join(100, 0);
				if ((threadInstances.isAlive() == false)
						&& !instanceThreadIsFinished) {
					instanceThreadIsFinished = true;
					printResult(resultsInstancesList);
				}

			} catch (InterruptedException e) {
				System.err.println("Thread for listing Instances is chrashed!");
			}

			try {
				threadVolumes.join(100, 0);
				if ((threadVolumes.isAlive() == false) && !volumeThreadIsFinished) {
					volumeThreadIsFinished = true;
					printResult(resultsVolumesList);
				}

			} catch (InterruptedException e) {
				System.err.println("Thread for listing Volumes is chrashed!");
			}

			try {
				threadKeys.join(100, 0);
				if ((threadKeys.isAlive() == false) && !keysThreadIsFinished) {
					keysThreadIsFinished = true;
					printResult(resultsKeysList);
				}

			} catch (InterruptedException e) {
				System.err.println("Thread for listing Keys is chrashed!");
			}
			
			
			try {
				threadLoadBalancers.join(100, 0);
				if ((threadLoadBalancers.isAlive() == false) && !loadBalancersThreadIsFinished) {
					loadBalancersThreadIsFinished = true;
					printResult(resultsLoadBalancersList);
				}

			} catch (InterruptedException e) {
				System.err.println("Thread for listing load balancers is chrashed!");
			}

			if (instanceThreadIsFinished && volumeThreadIsFinished
					&& keysThreadIsFinished && loadBalancersThreadIsFinished) {
				// all worker are finished
				break;
			}

			if (!instanceThreadIsFinished && !volumeThreadIsFinished
					&& !keysThreadIsFinished && !loadBalancersThreadIsFinished) {
				// no worker is finished yet
				System.out.print(".");
				System.out.flush();
			}
		}
		System.out.println(
				"\n====================================================\n");

		System.out.println("exit");
	}

	private static void findRegions(AmazonEC2 ec2)
	{
		DescribeRegionsResult regions_response = ec2.describeRegions();
		for (Region region : regions_response.getRegions()) {
			System.out.println("Found region " + region.getRegionName());

			// if(region.getRegionName().equals("us-west-2")) {
			// ec2Regions.add(region);
			// }
			ec2Regions.add(region);
		}
	}

	private static void printResult(ArrayList<String> resultsList)
	{
		for (String string : resultsList) {
			System.out.println(string);
		}
		System.out.flush();
	}
}
