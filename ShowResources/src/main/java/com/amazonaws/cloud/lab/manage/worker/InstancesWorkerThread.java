package com.amazonaws.cloud.lab.manage.worker;

import com.amazonaws.cloud.lab.manage.ShowResources;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Region;
import com.amazonaws.services.ec2.model.Reservation;

public class InstancesWorkerThread extends Thread
{

	public void run()
	{
		boolean leastOneActiveInstanceFound = false;
		ShowResources.resultsInstancesList.add(
				"\n====================================================\n EC2-Instances");

		for (Region region : ShowResources.ec2Regions) {
			boolean leastOneInstanceActivInRegions = false;
			boolean done = false;

			AmazonEC2 ec2Region = AmazonEC2ClientBuilder.standard()
					.withRegion(region.getRegionName()).build();

			DescribeInstancesRequest request = new DescribeInstancesRequest();
			while (!done) {
				DescribeInstancesResult response = ec2Region
						.describeInstances(request);
				for (Reservation reservation : response.getReservations()) {
					for (Instance instance : reservation.getInstances()) {

						if (leastOneInstanceActivInRegions == false) {
							ShowResources.resultsInstancesList.add(
									"===> Region: " + region.getRegionName() + " <===");
							leastOneInstanceActivInRegions = true;
							leastOneActiveInstanceFound = true;
						}

						if (instance.getState().getName().equals("running")) {
							// running
							ShowResources.resultsInstancesList
									.add("Running instance: " + instance.getInstanceId()
											+ " found!");

						} else if (instance.getState().getName().equals("stopped")) {
							// not running
							ShowResources.resultsInstancesList
									.add("Stopped instance: " + instance.getInstanceId()
											+ " found!");
						}
					}
				}
				request.setNextToken(response.getNextToken());

				if (response.getNextToken() == null) {
					done = true;
				}
			}
		}

		if (leastOneActiveInstanceFound == false) {
			ShowResources.resultsInstancesList
					.add("No Instance in all regions found!");
		}
	}
}
