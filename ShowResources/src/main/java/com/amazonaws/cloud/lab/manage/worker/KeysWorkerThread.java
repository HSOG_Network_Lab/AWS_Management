package com.amazonaws.cloud.lab.manage.worker;

import com.amazonaws.cloud.lab.manage.ShowResources;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeKeyPairsRequest;
import com.amazonaws.services.ec2.model.DescribeKeyPairsResult;
import com.amazonaws.services.ec2.model.KeyPairInfo;
import com.amazonaws.services.ec2.model.Region;

public class KeysWorkerThread extends Thread
{

	public void run()
	{

		boolean leastOneKeyFound = false;

		ShowResources.resultsKeysList.add(
				"\n====================================================\n Key Pair");

		for (Region region : ShowResources.ec2Regions) {

			boolean leastOneKeyFoundInRegion = false;

			AmazonEC2 ec2Region = AmazonEC2ClientBuilder.standard()
					.withRegion(region.getRegionName()).build();
			DescribeKeyPairsRequest request = new DescribeKeyPairsRequest();

			DescribeKeyPairsResult responese = ec2Region.describeKeyPairs(request);

			for (KeyPairInfo keyPair : responese.getKeyPairs()) {
				leastOneKeyFound = true;

				if (leastOneKeyFoundInRegion == false) {
					ShowResources.resultsKeysList
							.add("===> Region: " + region.getRegionName() + " <===");

					leastOneKeyFoundInRegion = true;
				}
				ShowResources.resultsKeysList
						.add("Found key pair: " + keyPair.getKeyName());
			}
		}

		if (leastOneKeyFound == false) {
			ShowResources.resultsKeysList.add("No keys in all regions found!");
		}
	}
}
