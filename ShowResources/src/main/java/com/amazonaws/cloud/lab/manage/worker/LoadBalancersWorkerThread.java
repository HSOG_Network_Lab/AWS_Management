package com.amazonaws.cloud.lab.manage.worker;

import com.amazonaws.cloud.lab.manage.ShowResources;
import com.amazonaws.services.ec2.model.Region;
import com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancing;
import com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancingClient;
import com.amazonaws.services.elasticloadbalancing.model.LoadBalancerDescription;

public class LoadBalancersWorkerThread extends Thread
{

	public void run()
	{
		boolean leastOneActiveLoadBalancerFound = false;
		ShowResources.resultsLoadBalancersList.add(
				"\n====================================================\n Load Balancers");

		for (Region region : ShowResources.ec2Regions) {
			boolean leastOneInstanceActivInRegions = false;

			AmazonElasticLoadBalancing elbv2 = AmazonElasticLoadBalancingClient
					.builder().withRegion(region.getRegionName()).build();

			com.amazonaws.services.elasticloadbalancing.model.DescribeLoadBalancersResult response = elbv2
					.describeLoadBalancers();

			for (LoadBalancerDescription lb : response
					.getLoadBalancerDescriptions()) {

				if (leastOneInstanceActivInRegions == false) {
					ShowResources.resultsLoadBalancersList
							.add("===> Region: " + region.getRegionName() + " <===");
					leastOneInstanceActivInRegions = true;
					leastOneActiveLoadBalancerFound = true;
				}
				ShowResources.resultsLoadBalancersList
						.add("Found Load Balancer: " + lb.getDNSName());
			}
		}

		if (leastOneActiveLoadBalancerFound == false) {
			ShowResources.resultsLoadBalancersList
					.add("No Load Balancers in all regions found!");
		}
	}
}
