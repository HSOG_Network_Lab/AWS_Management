package com.amazonaws.cloud.lab.manage.worker;

import com.amazonaws.cloud.lab.manage.ShowResources;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeVolumesRequest;
import com.amazonaws.services.ec2.model.DescribeVolumesResult;
import com.amazonaws.services.ec2.model.Region;
import com.amazonaws.services.ec2.model.Volume;

public class VolumesWorkerThread extends Thread
{

	public void run()
	{

		boolean leastOneVolumeFound = false;

		ShowResources.resultsVolumesList.add(
				"\n====================================================\n Volumes (Non-Attached)");

		for (Region region : ShowResources.ec2Regions) {

			boolean leastOneVolumeInRegion = false;

			AmazonEC2 ec2Region = AmazonEC2ClientBuilder.standard()
					.withRegion(region.getRegionName()).build();
			DescribeVolumesRequest request = new DescribeVolumesRequest();
			DescribeVolumesResult responese = ec2Region.describeVolumes(request);

			for (Volume vol : responese.getVolumes()) {
				
				//only the non attached to an EC2 instance
				if(vol.getAttachments().size() == 0)
				{				
				leastOneVolumeFound = true;

				if (leastOneVolumeInRegion == false) {
					ShowResources.resultsVolumesList
							.add("===> Region: " + region.getRegionName() + " <===");

					leastOneVolumeInRegion = true;
				}
				
				
				
				
				ShowResources.resultsVolumesList
						.add(" Volume: " + vol.getVolumeId());
				}
				

			}
		}
		if (leastOneVolumeFound == false) {
			ShowResources.resultsVolumesList
					.add("No volumes in all regions found!");
		}
	}
}
