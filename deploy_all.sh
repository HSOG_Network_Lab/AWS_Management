#!/usr/bin/env bash 

echo "starting deployment ..."

versionfile="VERSION"     #the file where you keep your string name
read -d $'\x04' versionstr < "$versionfile" #the content of $file is redirected to stdin from where it is read out into the $name variable
argument="s/1.0-SNAPSHOT/${versionstr}/"



#deploy ShowResources
cp ShowResources/pom.xml ShowResources/pom.xml.old
sed -i "${argument}" ShowResources/pom.xml
cd ShowResources
rm -f target/*.jar
mvn assembly:assembly -DdescriptorId=jar-with-dependencies
cd ..
rm ShowResources/pom.xml
mv ShowResources/pom.xml.old ShowResources/pom.xml 



#deploy CleanResources
cp CleanResources/pom.xml CleanResources/pom.xml.old
sed -i "${argument}" CleanResources/pom.xml
cd CleanResources
rm -f target/*.jar
mvn assembly:assembly -DdescriptorId=jar-with-dependencies
cd ..
rm CleanResources/pom.xml
mv CleanResources/pom.xml.old CleanResources/pom.xml 




echo "exit"
